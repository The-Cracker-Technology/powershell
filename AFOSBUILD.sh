rm -rf /opt/ANDRAX/powershell

mkdir powershell

if [ $(uname -m | grep 'x86_64') ]; then
   wget https://github.com/PowerShell/PowerShell/releases/download/v7.4.5/powershell-7.4.5-linux-x64.tar.gz

   if [ $? -eq 0 ]
   then
     # Result is OK! Just continue...
     echo "Download Powershell... PASS!"
   else
     # houston we have a problem
     exit 1
   fi

   tar -xvzf powershell-7.4.5-linux-x64.tar.gz -C powershell

   if [ $? -eq 0 ]
   then
     # Result is OK! Just continue...
     echo "Extract Powershell... PASS!"
   else
     # houston we have a problem
     exit 1
   fi
else
   wget https://github.com/PowerShell/PowerShell/releases/download/v7.4.5/powershell-7.4.5-linux-arm64.tar.gz

   if [ $? -eq 0 ]
   then
     # Result is OK! Just continue...
     echo "Download Powershell... PASS!"
   else
     # houston we have a problem
     exit 1
   fi

   tar -xvzf powershell-7.4.5-linux-arm64.tar.gz -C powershell

   if [ $? -eq 0 ]
   then
     # Result is OK! Just continue...
     echo "Extract Powershell... PASS!"
   else
     # houston we have a problem
     exit 1
   fi
fi

cp -Rf powershell /opt/ANDRAX/

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
